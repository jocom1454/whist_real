/*
Student Number - 100211671
Author - ukk17cbu
This is the BasicPlayer class for the Whilst game. This is how a player needs to
be created.
 */
package whist;

import cards.*;
import cards.Card.CompareRank;
import cards.Card.Suit;
import java.util.Collections;

public class BasicPlayer implements Player {

    //This is a declaration of the atttribtes
    public Hand playersHand;
    Strategy strategy;
    public Player currentPlayer;
    public int pID;
    public int partnerID;
    public Suit trumps;

    //This is a constructor for a BasicPlayer
    public BasicPlayer(int pID, Hand playersHand, Strategy strategy) {
        //A player needs a hand, a strategy and an ID
        this.playersHand = playersHand;
        this.strategy = strategy;
        this.pID = pID;
    }

    //This will set their ID
    public int setID(Trick t) {
        //This is a for loop that will iterate through the players array in the Trick class
        for (int i = 0; i < t.players.length; i++) {
            //The playerID is concurrent to the position of the player
            pID = i;
        }
        //This will return the ID of the player
        return pID;
    }

    @Override
    //This will add the card from the deal in the BasicGame class to the players hand
    public void dealCard(Card c) {
        playersHand.hand1.add(c);
    }

    @Override
    //This will allow for an external setting of the players strategy
    public void setStrategy(Strategy s) {
        strategy = s;
    }

    @Override
    //This is what will occur when the player plays a card
    public Card playCard(Trick t) {
        //This prints the playersID
        System.out.println("Player - " + pID);
        //This will set the card to be played to be set by the chooseCard method in BasicStrategy
        Card c = strategy.chooseCard(playersHand, t);
        //This will sort the players hand by rank
        Collections.sort(playersHand.hand1, new CompareRank());
        System.out.println("The card being played is a - " + c);
        //This will play the card and remove it from the hand
        playersHand.hand1.remove(c);
        return c;

    }

    @Override
    //This allows for a player to see the trick
    public void viewTrick(Trick t) {
        //This is a for loop that will print all the cards in the trick
        for (Card cardsPlayed1 : t.cardsPlayed1) {
            System.out.println(cardsPlayed1);
        }
    }

    @Override
    //This will set the trumps
    public void setTrumps(Suit s) {
        s = Trick.trumps;
    }

    @Override
    //This will return the players ID
    public int getID() {
        return pID;
    }
}
