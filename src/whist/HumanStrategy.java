/*
Student Number - 100211671
Author - ukk17cbu
This is the BasicStrategy class for the Whilst game. This is how a human player 
will be able to choose a card.
 */
package whist;

import cards.Card;
import cards.Card.CompareRank;
import cards.Hand;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author ukk17cbu
 */
public class HumanStrategy implements Strategy {

    //This is the card that will be returned by the chooseCard method
    Card cardToPlay;

    @Override
    public Card chooseCard(Hand h, Trick t) {
        //This is a null count variable
        int nullCount = 0;
        //This is a for loop that will find if there are cards that are not null in the trick
        for (Card cardsPlayed1 : t.cardsPlayed1) {
            if (cardsPlayed1 != null) {
                nullCount++;
            }
        }

        //If the player is the first player the nullCount will remain 0
        if (nullCount == 0) {
            //It will print this out
            System.out.println("You are the first player. Please pick a card");
            //The players hand will be sorted
            Collections.sort(h.hand1, new CompareRank());
            //This is a for loop that will loop through the hand and print it for the user
            for (int i = 0; i < h.hand1.size(); i++) {
                //This will print the hand in the format (i) - card
                System.out.println("(" + i + ")" + h.hand1.get(i));
            }
            //This is creating a new scanner object to scan the user input in
            Scanner sc = new Scanner(System.in);
            //This is a boolean value that will ensure the game does not crash
            boolean success = false;
            //While the boolean value is false
            while (!success) {
                //It will try to allow the input
                try {
                    System.out.println("Please enter a number from the above list");
                    //This will set the input to int j
                    int j = sc.nextInt();
                    //This is get the corresponding card
                    cardToPlay = h.hand1.get(j);
                    //This will then change the boolean value
                    success = true;
                } //If the input is not an integer
                catch (InputMismatchException e) {
                    //This will allow them to try again
                    sc.next();
                    System.out.println("You have not entered a number, please try again.");
                }
            }

        } //If they are not the first player
        else if (nullCount > 0) {
            //This is an arrayList that will hold the cards of the lead suit
            ArrayList<Card> suitCards = new ArrayList<>();
            //This loop will iterate through the hand
            for (int i = 0; i < h.hand1.size(); i++) {
                //If there are cards that are equal to the lead suit
                if (h.hand1.get(i).suit == t.leadCard.suit) {
                    //It will add them to the suitCards arrayList
                    suitCards.add(h.hand1.get(i));
                }
            }
            //This will sort the suitCards arrayList
            Collections.sort(suitCards, new CompareRank());
            //If the player has none of the leadSuit
            if (suitCards.isEmpty()) {
                t.getPartnerID();
                //It will sort the hand
                Collections.sort(h.hand1, new CompareRank());
                //It will check if the partner is winning the trick currently
                if (t.winningCardID == t.partnerID) {
                    //If the partner is winning it will tell the user and print the current trick
                    System.out.println("Your partner is winning currently but you do not have the lead suit");
                    System.out.println("The current trick is - ");
                    ArrayList<Card> notNull = new ArrayList<>();
                    //As the trick is an array, some values could be null
                    for (int i = 0; i < t.cardsPlayed1.length; i++) {
                        if (t.cardsPlayed1[i] != null) {
                            notNull.add(t.cardsPlayed1[i]);
                        }
                    }
                    //This will print all the cards in the trick that are not null
                    System.out.println(notNull);
                    System.out.println("Your hand currently is - ");
                    //This is a for loop that will loop through the hand and print it for the user
                    for (int i = 0; i < h.hand1.size(); i++) {
                        //This will print the hand in the format (i) - card
                        System.out.println("(" + i + ")" + h.hand1.get(i));
                    }
                    //This is creating a new scanner object to scan the user input in
                    Scanner sc = new Scanner(System.in);
                    //This is a boolean value that will ensure the game does not crash
                    boolean success = false;
                    //While the boolean value is false
                    while (!success) {
                        //It will try to allow the input
                        try {
                            System.out.println("Please enter a number from the above list");
                            //This will set the input to int j
                            int j = sc.nextInt();
                            //This is get the corresponding card
                            cardToPlay = h.hand1.get(j);
                            //This will then change the boolean value
                            success = true;
                        } //If the input is not an integer
                        catch (InputMismatchException e) {
                            //This will allow them to try again
                            sc.next();
                            System.out.println("You have not entered a number, please try again.");
                        }
                    }
                }//If their partner has not played or is not winning
                else if (t.winningCardID != t.partnerID || nullCount < 2) {
                    System.out.println("Your partner is not winning or has not played and you do not have the lead suit");
                    System.out.println("The current trick is - ");
                    ArrayList<Card> notNull = new ArrayList<>();
                    //It will print the current trick
                    for (Card cardsPlayed1 : t.cardsPlayed1) {
                        if (cardsPlayed1 != null) {
                            notNull.add(cardsPlayed1);
                        }
                    }
                    System.out.println(notNull);
                    System.out.println("Your hand currently is - ");
                    //It will print the current hand
                    for (int i = 0; i < h.hand1.size(); i++) {
                        System.out.println("(" + i + ")" + h.hand1.get(i));
                    }
                    //This is creating a new scanner object to scan the user input in
                    Scanner sc = new Scanner(System.in);
                    //This is a boolean value that will ensure the game does not crash
                    boolean success = false;
                    //While the boolean value is false
                    while (!success) {
                        //It will try to allow the input
                        try {
                            System.out.println("Please enter a number from the above list");
                            //This will set the input to int j
                            int j = sc.nextInt();
                            //This is get the corresponding card
                            cardToPlay = h.hand1.get(j);
                            //This will then change the boolean value
                            success = true;
                        } //If the input is not an integer
                        catch (InputMismatchException e) {
                            //This will allow them to try again
                            sc.next();
                            System.out.println("You have not entered a number, please try again.");
                        }
                    }
                }
            } //If the user does have the lead suit 
            else if (suitCards.size() > 0) {
                t.getPartnerID();
                //It will check if it's partner is winning
                if (t.winningCardID == t.partnerID) {
                    //If their partner is winning
                    System.out.println("Your partner is winning and you have the lead suit");
                    //It will print the current trick
                    System.out.println("The current trick is - ");
                    ArrayList<Card> notNull = new ArrayList<>();
                    for (Card cardsPlayed1 : t.cardsPlayed1) {
                        if (cardsPlayed1 != null) {
                            notNull.add(cardsPlayed1);
                        }
                    }
                    System.out.println(notNull);
                    //It will print all the cards that the user can currently play
                    System.out.println("The cards you can play currently are - ");
                    for (int i = 0; i < suitCards.size(); i++) {
                        //This will print the cards in the format (i) - card
                        System.out.println("(" + i + ")" + suitCards.get(i));
                    }
                    //This is creating a new scanner object to scan the user input in
                    Scanner sc = new Scanner(System.in);
                    //This is a boolean value that will ensure the game does not crash
                    boolean success = false;
                    //While the boolean value is false
                    while (!success) {
                        //It will try to allow the input
                        try {
                            System.out.println("Please enter a number from the above list");
                            //This will set the input to int j
                            int j = sc.nextInt();
                            //This is get the corresponding card
                            cardToPlay = suitCards.get(j);
                            //This will then change the boolean value
                            success = true;
                        } //If the input is not an integer
                        catch (InputMismatchException e) {
                            //This will allow them to try again
                            sc.next();
                            System.out.println("You have not entered a number, please try again.");
                        }
                    }

                } //If their partner is not winning or has not played
                else if (t.winningCardID != t.partnerID || nullCount < 2) {
                    System.out.println("Your partner is not winning or has not played");
                    //It will print the current trick
                    System.out.println("The current trick is - ");
                    ArrayList<Card> notNull = new ArrayList<>();
                    for (Card cardsPlayed1 : t.cardsPlayed1) {
                        if (cardsPlayed1 != null) {
                            notNull.add(cardsPlayed1);
                        }
                    }
                    System.out.println(notNull);
                    //It will print all the cards that the user can currently play
                    System.out.println("The cards you can play currently are - ");
                    for (int i = 0; i < suitCards.size(); i++) {
                        //This will print the cards in the format (i) - card
                        System.out.println("(" + i + ")" + suitCards.get(i));
                    }
                    //This is creating a new scanner object to scan the user input in
                    Scanner sc = new Scanner(System.in);
                    //This is a boolean value that will ensure the game does not crash
                    boolean success = false;
                    //While the boolean value is false
                    while (!success) {
                        //It will try to allow the input
                        try {
                            System.out.println("Please enter a number from the above list");
                            //This will set the input to int j
                            int j = sc.nextInt();
                            //This is get the corresponding card
                            cardToPlay = suitCards.get(j);
                            //This will then change the boolean value
                            success = true;
                        } //If the input is not an integer
                        catch (InputMismatchException e) {
                            //This will allow them to try again
                            sc.next();
                            System.out.println("You have not entered a number, please try again.");
                        }
                    }
                }
            }
        }
        return cardToPlay;
    }

    @Override
    public void updateData(Trick c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
