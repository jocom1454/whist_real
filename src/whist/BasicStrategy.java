/*
Student Number - 100211671
Author - ukk17cbu
This is the BasicStrategy class for the Whilst game. This is how a player will 
choose a card.
 */
package whist;

import cards.*;
import cards.Card.CompareRank;
import cards.Card.Suit;
import cards.Hand;
import java.util.ArrayList;
import java.util.Collections;

public class BasicStrategy implements Strategy {

    //This is a variable that will be returned by the chooseCard method
    Card cardToPlay;

    @Override
    public Card chooseCard(Hand h, Trick t) {
        //This is a null count variable
        int nullCount = 0;
        //This is a for loop that will find if there are cards that are not null in the trick
        for (Card cardsPlayed1 : t.cardsPlayed1) {
            if (cardsPlayed1 != null) {
                nullCount++;
            }
        }
        //If the trick is empty, all cards will be null so the nullCount will remain 0
        if (nullCount == 0) {
            //This will sort the players hand in rank
            Collections.sort(h.hand1, new CompareRank());
            //This is a for loop that will iterate through the loop
            for (int i = 0; i < h.hand1.size(); i++) {
                //If there is more than one card ranked as high as the highest rank
                if (h.hand1.get(h.hand1.size() - 1).rank == h.hand1.get(i).rank) {
                    ArrayList<Card> sameRank = new ArrayList<>();
                    //This will add all cards of the same rank to the arrayList
                    sameRank.add(h.hand1.get(i));
                    //This will shuffle the arrayList so that a random card can be picked
                    Collections.shuffle(sameRank);
                    //This sets the card to play as the first value in the array
                    cardToPlay = sameRank.get(0);
                } //If there is not two or more highest ranked cards 
                else {
                    //It will return the biggest card in the hand, the last value
                    cardToPlay = h.hand1.get(h.hand1.size() - 1);
                }
            }
        } //If there are cards in the trick
        else if (nullCount > 0) {
            ArrayList<Card> suitCards = new ArrayList<>();
            //This loop will iterate through the hand
            for (int i = 0; i < h.hand1.size(); i++) {
                //If there are cards that are equal to the lead suit
                if (h.hand1.get(i).suit == t.leadCard.suit) {
                    //It will add them to the suitCards arrayList
                    suitCards.add(h.hand1.get(i));
                }
            }
            //This will sort the suitCards arrayList in rank order
            Collections.sort(suitCards, new CompareRank());
            //If there are cards in the suitCards arrayList
            //If the player does not have any of the suit 
            if (suitCards.isEmpty()) {
                //This is calling the get partner ID method
                t.getPartnerID();
                //It will sort the hand by rank
                Collections.sort(h.hand1, new CompareRank());
                //If the winning card is my partners
                if (t.winningCardID == t.partnerID/*of my partner*/) {
                    ArrayList<Card> nonTrumpCards = new ArrayList<>();
                    //This will loop through the hand
                    for (int i = 0; i < h.hand1.size(); i++) {
                        //If the card is not a trumps
                        if (h.hand1.get(i).suit != Trick.trumps) {
                            //It will add them to the arrayList
                            nonTrumpCards.add(h.hand1.get(i));
                        } //This will set the cardToPlay to the smallest card
                        
                    } //If all they have are trumps
                    if (nonTrumpCards.isEmpty()) {
                        //It will play a low trumps
                        cardToPlay = h.hand1.get(0);
                    } else {
                        cardToPlay = nonTrumpCards.get(0);
                    }

                } //If my partner is not winning or hasn't played
                
                else if (t.winningCardID != t.partnerID/*of my partner*/ || nullCount < 2) {
                    ArrayList<Card> possibleCards = new ArrayList<>();
                    ArrayList<Card> trumpCards = new ArrayList<>();
                    ArrayList<Card> nonTrumpCards = new ArrayList<>();
                    //This will loop through the hand
                    for (int i = 0; i < h.hand1.size(); i++) {
                        //This will find all the trump cards in the hand
                        if (h.hand1.get(i).suit == Trick.trumps) {
                            trumpCards.add(h.hand1.get(i));
                            //This will find all the non trump cards in the hand
                        } else if (h.hand1.get(i).suit != Trick.trumps) {
                            nonTrumpCards.add(h.hand1.get(i));
                        }
                    }
                    //This will sort both array lists
                    Collections.sort(nonTrumpCards, new CompareRank());
                    Collections.sort(trumpCards, new CompareRank());
                    //This is calling the getWinningCard method 
                    t.getWinningCard();
                    if (t.winningCard.suit == Trick.trumps) {
                        for (int i = 0; i < trumpCards.size(); i++) {
                            //This will compare the winning card to the trumpCards array
                            int x = new CompareRank().compare(t.winningCard, trumpCards.get(i));
                            if (x == 1) {
                                if (nonTrumpCards.isEmpty()) {
                                    cardToPlay = h.hand1.get(0);
                                } else {
                                    cardToPlay = nonTrumpCards.get(0);
                                }
                            } else if (x == -1) {
                                possibleCards.add(trumpCards.get(i));

                            }
                            Collections.sort(possibleCards, new CompareRank());
                            if (possibleCards.isEmpty()) {
                                cardToPlay = h.hand1.get(0);
                            } else {
                                cardToPlay = possibleCards.get(possibleCards.size() - 1);
                            }
                        }
                    } else if (t.winningCard.suit != Trick.trumps) {
                        if (nonTrumpCards.isEmpty()) {
                            cardToPlay = h.hand1.get(0);
                        } else {
                            cardToPlay = nonTrumpCards.get(0);
                        }
                    } else if (nonTrumpCards.isEmpty()) {
                        cardToPlay = trumpCards.get(0);
                    }
                }
            } //If they do have the lead suit in their hand
            else if (suitCards.size() > 0) {
                t.getPartnerID();
                //They will check if their partner is winning
                if (t.winningCardID == t.partnerID/*of my partner*/) {
                    //If their partner is winning, it will play the lowest card possible, following suit
                    cardToPlay = suitCards.get(0);
                } //They will check if their partner is not winning or hasn't played
                else if (t.winningCardID != t.partnerID/*of my partner*/ || nullCount < 2) {
                    //This will loop through the suitCards arrayList
                    ArrayList<Card> possibleCards = new ArrayList<>();
                    for (int i = 0; i < suitCards.size(); i++) {

                        //This will compare their cards of the suit to the winningCard
                        //Need to check if the winningCard.suit is equal to the lead suit
                        int x = new CompareRank().compare(t.winningCard, suitCards.get(i));
                        //If the card is bigger
                        if (x == -1) {
                            //It will add all the cards bigger than the winningCard
                            possibleCards.add(suitCards.get(i));
                            //This will set the cardToPlay to the biggest card possible
                            cardToPlay = possibleCards.get(possibleCards.size() - 1);
                        } //If the winningCard is bigger
                        else if (x == 1) {
                            //It will add all the cards smaller than the winning card
                            possibleCards.add(suitCards.get(i));
                            //This will then play the lowest card possible
                            cardToPlay = possibleCards.get(0);
                        }
                    }
                }
            }
        }
        return cardToPlay;
    }

    @Override
    public void updateData(Trick c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
