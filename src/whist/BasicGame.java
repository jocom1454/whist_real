/*
Student Number - 100211671
Author - ukk17cbu
This is the BasicGame class for the Whist game. This is how a game will be
played.
 */
package whist;

import cards.Deck;
import cards.Card.Suit;
import cards.Hand;

public class BasicGame {

    static final int NOS_PLAYERS = 4;
    static final int NOS_TRICKS = 13;
    static final int WINNING_POINTS = 7;
    int team1Points = 0;
    int team2Points = 0;
    static int team1Total = 0;
    static int team2Total = 0;
    Player[] players;

    //This set the players
    public BasicGame(Player[] pl) {
        players = pl;

    }

    //This deals the hands for each player using deal from Deck
    public void dealHands(Deck newDeck) {
        for (int i = 0; i < 52; i++) {
            players[i % NOS_PLAYERS].dealCard(newDeck.deal());
        }
    }

    //This plays a single trick
    public Trick playTrick(Player firstPlayer) {
        Trick t = new Trick(firstPlayer.getID());
        int playerID = firstPlayer.getID();
        for (int i = 0; i < NOS_PLAYERS; i++) {
            int next = (playerID + i) % NOS_PLAYERS;
            t.setCard(players[next].playCard(t), players[next]);
        }
        return t;
    }

    //This plays an entire game
    public void playGame() {
        Deck d = new Deck();
        dealHands(d);
        int firstPlayer = (int) (NOS_PLAYERS * Math.random());
        //This will set the trumps
        Suit trumps = Suit.getRandomSuit();
        Trick.setTrumps(trumps);
        for (int i = 0; i < NOS_PLAYERS; i++) {
            players[i].setTrumps(trumps);
        }
        int team1tricks = 0;
        int team2tricks = 0;
        //This will play 13 seasons
        for (int i = 0; i < NOS_TRICKS; i++) {
            System.out.println("NEW TRICK");
            System.out.println("The trumps for this round is - " + trumps);
            Trick t = playTrick(players[firstPlayer]);
            //This will print the trick
            System.out.println("Trick = " + "\n" + t);
            //This will find the winner
            int winningPlayer = t.findWinner();
            System.out.println("Winner = " + winningPlayer + "\n");
            //This will get the team points
            if (t.winningCardID == 0) {
                team1tricks++;
            }
            if (t.winningCardID == 1) {
                team2tricks++;
            }
            if (t.winningCardID == 2) {
                team1tricks++;
            }
            if (t.winningCardID == 3) {
                team2tricks++;
            }
            System.out.println("Team 1 tricks are - " + team1tricks);
            System.out.println("Team 2 tricks are - " + team2tricks);
            System.out.println("------------------------------");
        }

        if (team1tricks > 6) {
            team1Points += team1tricks - 6;
        }
        if (team2tricks > 6) {
            team2Points += team2tricks - 6;
        }

    }

    /**
     * Method to find the winner of a trick. Note
     *
     * @param t: current trick
     * @return the index of the winning player
     */
    public void playMatch() {
        team1Points = 0;
        team2Points = 0;
        while (team1Points < WINNING_POINTS && team2Points < WINNING_POINTS) {
            System.out.println("NEW GAME!!!");
            playGame();
            System.out.println("Team 1 total points is " + team1Points);
            System.out.println("Team 2 total points is " + team2Points);
        }
        if (team1Points >= WINNING_POINTS) {
            System.out.println("Winning team is team 1 with " + team1Points);
            team1Total++;
        } else {
            System.out.println("Winning team is team 2 with " + team2Points);
            team2Total++;
        }

        System.out.println("");
        System.out.println("Team 1 Total Wins: " + team1Total);
        System.out.println("Team 2 Total Wins: " + team2Total);
        System.out.println("");

    }

    public static void playTestGame() {
        //This is setting the players
        Player[] p = new Player[NOS_PLAYERS];
        for (Player p1 : p) {
            p[0] = new BasicPlayer(0, new Hand(), new BasicStrategy());
            p[1] = new BasicPlayer(1, new Hand(), new BasicStrategy());
            //This is a test for the HumanStrategy player
//            p[2] = new BasicPlayer(2, new Hand(), new HumanStrategy());
            p[2] = new BasicPlayer(2, new Hand(), new BasicStrategy());
            p[3] = new BasicPlayer(3, new Hand(), new BasicStrategy());
        }
        BasicGame bg = new BasicGame(p);
        bg.playMatch(); //Just plays a single match
    }

    public static void main(String[] args) {
        playTestGame();
    }

}
