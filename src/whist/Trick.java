/*
Student Number - 100211671
Author - ukk17cbu
This is the Trick class for the Whilst game. This will run a single trick.
 */
package whist;

import cards.Card;
import cards.Card.Suit;
import cards.Card.CompareRank;
import cards.Card.Rank;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Trick {

    //This is a declaration of all the attributes for the
    public static Suit trumps;
    public Card[] cardsPlayed1;
    public Player[] players;
    int leadPlayer;
    public Card leadCard;
    public Card winningCard;
    int winningCardID;
    int partnerID;

    //This is the constructor for a trick
    public Trick(int p) {
        //p is the leadPlayer
        leadPlayer = p;
        //This is a card array of size 4 that is the trick
        cardsPlayed1 = new Card[4];
        players = new Player[4];

    }    //p is the lead player 

    //This is the declaration of the trump suit decided on in the basicGame class
    public static void setTrumps(Suit s) {
        trumps = s;
    }

    //This will return the lead player
    public int getLeadPlayer() {
        return leadPlayer;
    }
    
    //This will return the partnerID
    public int getPartnerID() {
        for (int i = 0; i < players.length; i++) {
            switch (i) {
                case 0:
                    partnerID = 2;
                    break;
                case 1:
                    partnerID = 3;
                    break;
                case 2:
                    partnerID = 0;
                    break;
                case 3:
                    partnerID = 1;
                    break;
                default:
                    break;
            }
        }
        return partnerID;
    }

    /**
     *
     * @return the Suit of the lead card.
     */
    public Suit getLeadSuit() {
        return leadCard.suit;
    }

    public Card getWinningCard() {
        return winningCard;
    }

    /**
     * Records the Card c played by Player p for this trick
     *
     * @param c
     * @param p
     */
    public void setCard(Card c, Player p) {
        //This adds the player in the position of their playerID
        players[p.getID()] = p;
        //This sets the card played by the player in the position of the player
        cardsPlayed1[p.getID()] = c;
        //If they are the first player, c will become the leadCard
        if (p.getID() == leadPlayer) {
            leadCard = c;
        }
        //This will find the winning card of a trick at any point
        winningCard = cardsPlayed1[findWinner()];
    }

    /**
     * Returns the card played by player with id p for this trick
     *
     * @param p
     * @return
     */
    public Card getCard(Player p) {
        return cardsPlayed1[p.getID()];
    }

    /**
     * Finds the ID of the winner of a completed trick
     *
     * @return
     */
    public int findWinner() {
        //These are some arrayLists that I will use further down in my method
        ArrayList<Card> trueTricks = new ArrayList<>();
        ArrayList<Card> leadingSuit = new ArrayList<>();
        ArrayList<Card> trumpSuit = new ArrayList<>();
        //This method can be called at any time, it can be called on a non full trick
        for (Card cardsPlayed11 : cardsPlayed1) {
            //This means that I will loop through the array and find any null values
            if (cardsPlayed11 != null) {
                //If there are cards in the array, this will add it to the trueTricks AL
                trueTricks.add(cardsPlayed11);
            }
        }
        //This will iterate through the arrayList
        for (int i = 0; i < trueTricks.size(); i++) {
            //If the lead suit is not equal to the trumps
            if (leadCard.suit != Trick.trumps) {
                //If the current card in the arrayList is equal to the lead suit
                if (trueTricks.get(i).suit == leadCard.suit) {
                    //It will add it to the leadingSuit arrayList
                    leadingSuit.add(trueTricks.get(i));
                } //If the suit is a trumps suit
                else if (trueTricks.get(i).suit == Trick.trumps) {
                    //It will be added to the trumpSuit arrayList
                    trumpSuit.add(trueTricks.get(i));
                }
            } //If the lead suit is the trumps suit
            else if (leadCard.suit == Trick.trumps) {
                //If the current cards suit is equal to the trumps suit
                if (trueTricks.get(i).suit == Trick.trumps) {
                    //It will add it to the trumpSuit arrayList
                    trumpSuit.add(trueTricks.get(i));
                }
            }
        }
        //This will sort the arrayList's in rank order
        Collections.sort(leadingSuit, new CompareRank());
        Collections.sort(trumpSuit, new CompareRank());
        //If trumpSuit is empty
        if (trumpSuit.isEmpty()) {
            //The winning card will be the highest card in the leadingSuit arrayList
            winningCard = leadingSuit.get(leadingSuit.size() - 1);
        } //If the trumpSuit arrayList is not empty
        else if (trumpSuit.size() > 0) {
            //The winning card will be the biggest trump card
            winningCard = trumpSuit.get(trumpSuit.size() - 1);
        }

        //This will loop through the trick
        for (int i = 0; i < cardsPlayed1.length; i++) {
            //To find the place where the lead card is
            if (winningCard.equals(cardsPlayed1[i])) {
                //Once the lead card is found, it will get the winningcardID
                winningCardID = i;
            }
        }
        //This will return the winningCardID positon in the players arrayList
        return winningCardID;
    }

    @Override
    //This is a toString that will call the toString for each card from the Card class
    public String toString() {
        //This is a new StringBuilder object that will allow us to create a toString for a trick
        StringBuilder str = new StringBuilder();
        //This will iterate through the trick
        for (Card c : cardsPlayed1) {
            //This will call the toString form Card that will fashion how the trick is to be printed
            str.append(c.toString());

            //This is just adding a new line after each iteration
            str.append("\n");
        }
        //This will return the actual toString object created
        return str.toString();
    }

}
