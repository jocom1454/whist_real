/*
Student Number - 100211671
Author - ukk17cbu
This is the card class for the Whilst game. I will use enums to create Rank and
a Suit and will create a card object. This is a serialised class.
 */
package cards;

import java.io.Serializable;
import static cards.Card.Suit.getRandomSuit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class Card implements Serializable, Comparable<Card> {

    //This is the delcaration of the attributes
    private static final long serialVersionUID = 100L;
    public Rank rank;
    public Suit suit;

    //This is a constructor that will decide on how a card is made
    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    //These are the accessor methods for this class
    public Rank getRank() {
        return this.rank;
    }

    public Suit getSuit() {
        return this.suit;
    }

    //This is the declaration of the enum Rank
    public enum Rank {
        TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), JACK(10), QUEEN(10), KING(10), ACE(11);

//This is the declaration of the values of each variable of the Rank 
        int value;

//This is a contstructor that highlights what the Rank is made of
        Rank(int v) {
            value = v;
        }

        //This method gets the value of the rank of the card
        public int getValue() {
            return value;
        }

        //This method get the next value 
        public Rank getNext() {
            //This creates a variable that equals to the next value in the enum list
            int index = (this.ordinal() + 1) % Rank.values().length;
            //This returns the index which is the next value
            return Rank.values()[index];
        }
    }

//This is the declaration of the enum Suit
    public enum Suit {
        //These are all the suits that will be used in this enum
        CLUBS, DIAMONDS, HEARTS, SPADES;

        //This is a method that will choose a random suit anytime it is called upon
        public static Suit getRandomSuit() {
            //This line will create a new random object
            Random random = new Random();
            //This will return a random suit from the ones outlined above using the random object
            return values()[random.nextInt(values().length)];
        }
    }

    //This is a toString that will define how a card will be printed
    @Override
    public String toString() {

        return rank + " of " + suit;
    }

    //This is a compareTo method that will allow us to sort the cards 
    @Override
    public int compareTo(Card t) {
        //This line creates a variable and will compare the ranks
        int compare = Integer.compare(this.rank.ordinal(), t.rank.ordinal());
        //If the ranks are equal it will then compare based on suit
        if (compare == 0) {
            Integer.compare(this.suit.ordinal(), t.suit.ordinal());
        }
        //This returns a value of either, 1 if it is bigger, -1 if it is smaller and 0 if it is the same.
        return compare;
    }

    //This is a static nested class that will sort the list of cards into descending order
    public static class CompareDescending implements Comparator<Card> {

        @Override
        public int compare(Card t, Card t1) {
            //This line creates a variable and will compare the ranks
            int compare = Integer.compare(t1.rank.ordinal(), t.rank.ordinal());
            //If the ranks are equal it will then compare based on suit
            if (compare == 0) {
                Integer.compare(t1.suit.ordinal(), t.suit.ordinal());
            }
            //This returns a value of either, 1 if it is bigger, -1 if it is smaller and 0 if it is the same.
            return compare;
        }

    }

    //This is a static nested class that will sort the list of cards by rank alone
    public static class CompareRank implements Comparator<Card> {

        @Override
        //This will compare the two cards via rank alone
        public int compare(Card t, Card t1) {
            //This will return the comparable value of the values when compared
            int x = t.rank.ordinal() - t1.rank.ordinal();
            if (x > 0) {
                x = 1;
            } else if (x > 0) {
                x = -1;
            } else if (x == 0) {
                x = 1;
            }
            return x;

//return (((Rank) t1.rank).getValue() - ((Rank) t.rank).getValue());
        }
    }

    //This method will take an array of cards, use a comparator and a card to input the greater cards than the card that was input
    public static List<Card> chooseGreater(List<Card> a, Comparator b, Card c) {
        //This declares the ArrayList that will be declared
        ArrayList<Card> x = new ArrayList<>();
        //This for loop compares every card to the current card
        for (int i = 0; i < a.size(); i++) {
            //If the card is deemed bigger it will be added to the ArrayList x
            if (b.compare(c, a.get(i)) < 0) {
                x.add(a.get(i));
            }
        }
        //This returns the ArrayList of bigger cards than the card input
        return x;
    }

    //This will use my compareTo to find the highest card in a list 
    public static Card max(ArrayList<Card> maximumCard) {
        Card maximumCards = maximumCard.get(0);
        for (Card card : maximumCard) {
            if (maximumCards.compareTo(card) < 0) {
                maximumCards = card;
            }
        }
        return maximumCards;
    }

    //This is a method that allows you to use a card input and the array and compare them
    public static void selectTest() {
        //This is a comparator that will compare the cards by rank and suit
        Comparator<Card> cmp = (Card x, Card y) -> {
            //This returns the cards bigger than the current card
            return x.compareTo(y);
        };
        //This is the declaration of the array of cards
        ArrayList<Card> card = new ArrayList<>();
        Card card1 = new Card(Rank.EIGHT, Suit.CLUBS);
        Card card2 = new Card(Rank.EIGHT, Suit.HEARTS);
        Card card3 = new Card(Rank.SIX, Suit.HEARTS);
        Card card4 = new Card(Rank.NINE, Suit.DIAMONDS);
        Card card5 = new Card(Rank.QUEEN, Suit.HEARTS);
        Card card6 = new Card(Rank.JACK, Suit.HEARTS);
        card.add(card1);
        card.add(card2);
        card.add(card3);
        card.add(card4);
        card.add(card5);
        card.add(card6);
        Card cardA = new Card(Rank.EIGHT, Suit.CLUBS);
        //This will print the greater cards by both rank and suit
        System.out.println(chooseGreater(card, cmp, cardA));
        //This prints the cards smaller than the card input
        System.out.println(chooseGreater(card, new CompareDescending(), cardA));
        //This prints the cards with greater rank regardless of suit
        System.out.println(chooseGreater(card, new CompareRank(), cardA));
    }

    public static void main(String[] args) {

        //This is the declaration of the array of cards that will be used for testing
//        ArrayList<Card> card = new ArrayList<>();
//        Card card1 = new Card(Rank.EIGHT, Suit.CLUBS);
//        Card card2 = new Card(Rank.ACE, Suit.HEARTS);
//        Card card3 = new Card(Rank.EIGHT, Suit.HEARTS);
//        Card card4 = new Card(Rank.NINE, Suit.DIAMONDS);
//        Card card5 = new Card(Rank.QUEEN, Suit.HEARTS);
//        Card card6 = new Card(Rank.JACK, Suit.HEARTS);
//        card.add(card1);
//        card.add(card2);
//        card.add(card3);
//        card.add(card4);
//        card.add(card5);
//        card.add(card6);

        //This is a for-loop that will iterate through the array and print it out       
//        for (int i = 0; i < card.size(); i++) {
            //This prints out the sorted array
//            System.out.println(card.get(i));
//        }
        //This is a print line that prints a single space
//        System.out.println("\n");

        //This is a for loop that will loop through the array of cards and print the information out
//        for (int i = 0; i < card.size(); i++) {

            //This calls the getValue method from the Rank and the value for the rank of a card
//            card1.rank.getValue();
            //This calls the getNext value from the Rank enum, it gets the next rank for the current card
//            card1.rank.getNext();
            //This prints out the rank and siut of each card based on the ToString above
//            System.out.println("\n" + card.get(i) + " \nThe value of this card is - " + card.get(i).rank.value + "\nThis is the next Rank - " + card.get(i).rank.getNext());
//        }

        //This is calling the getRandomSuit method from the Suit enum, it will print a random suit
        //System.out.println(Suit.getRandomSuit());
        //This is calling the compareTo that compares the cards to each other and compares them by rank and suit
        //Collections.sort(card);
        //This is calling the method from the static nested class called CompareDescending
        //Card.CompareDescending nested = new Card.CompareDescending();
        //This is calling the CompareDescending compare method that will sort the ArrayList and put it in descending order
        //Collections.sort(card, nested);
        //This is calling the compareRank method from the static nested class called CompareRank
//        System.out.println("space \n");
//        Card.CompareRank nested1 = new Card.CompareRank();
//
//        Card new2 = new Card(Rank.EIGHT, Suit.CLUBS);
//        Card new1 = new Card(Rank.KING, Suit.CLUBS);
//
//        ArrayList<Card> card41 = new ArrayList<>();
//        card41.add(new1);
//        card41.add(new2);
//
//        Collections.sort(card41, nested1);

        //This is calling the CompareRank compare method that will sort the ArrayList and put it in order by rank
//        Collections.sort(card, nested1);
//        System.out.println(card41);

        //This returns the selectTest method that uses a simple comparator, chooseRank, compareDescending and CompareRank
        //selectTest();
    }

}
